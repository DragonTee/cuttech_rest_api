<?php

namespace App\Http\Controllers;

use App\Models\Specification;
use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SpecificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $product_id)
    {
        $page = $request->input('page', 0);
        $pageSize = $request->input('pageSize', 10);
        return Product::findOrFail($product_id)->specifications()->skip($page*$pageSize)->take($pageSize)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $product_id)
    {
        $data = $request->all();
        $data['product_id'] = $product_id;
        $specification = Specification::create($data);

        return response()->json($specification, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Specification  $specification
     * @return \Illuminate\Http\Response
     */
    public function show(Specification $specification)
    {
        return $specification;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Specification  $specification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Specification $specification)
    {
        $specification->update($request->all());

        return response()->json($specification, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Specification  $specification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Specification $specification)
    {
        $specification->delete();

        return response()->json(null, 204);
    }
}
