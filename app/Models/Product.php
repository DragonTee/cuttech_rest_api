<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    /**
     * Get all specifications of the product
     */
    public function specifications()
    {
        return $this->hasMany(Specification::class);
    }
    
    protected $fillable = [
        'name',
        'image_url',
        'category_id',
        'description',
        'price',
    ];
}
